## Setup
```
sudo apt install git
sudo git clone https://gitlab.com/remyj38/rpi-clock.git /opt/rpi-clock
cd /opt/rpi-clock
sudo bash rpi-clock-update.sh
```

## Electrical diagram
![electrical diagram](img/electrical_diagram.svg "Electrical Diagram")

Source (easyEDA): [electrical diagram.json](electrical_diagram.json)
