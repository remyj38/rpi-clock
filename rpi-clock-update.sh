#!/bin/bash

set -e

BASE_PATH=$(dirname $(readlink -f $0))
cd $BASE_PATH
# Detect if update needed
if [ -f /usr/local/bin/rpi-clock ]; then
    git fetch > /dev/null
    local=$(git rev-parse @)
    remote=$(git rev-parse @{u})
    base=$(git merge-base @ @{u})
    if [ $local = $remote ]; then
        echo "Nothing to do !"
        exit 0
    elif [ $local = $base ]; then
        git checkout . > /dev/null
        git clean -fd > /dev/null
        git pull > /dev/null
    elif [ $remote = $base ]; then
        echo "Remote is outdated"
        exit 1
    else
        echo "Branches are diverged"
        exit 1
    fi
fi

# Install requirements
apt-get install -y libgpiod2 python3 python3-pip i2c-tools python-smbus > /dev/null

# Create/Update user
set +e
id rpi-clock > /dev/null 2>&1
user_exists=$?
set -e
if [ $user_exists -ne 0 ]; then
    useradd -d $BASE_PATH -G gpio,i2c,spi -M -r -s /usr/sbin/nologin rpi-clock > /dev/null
else
    usermod -d $BASE_PATH -G gpio,i2c,spi -s /usr/sbin/nologin rpi-clock > /dev/null
fi

chown -R rpi-clock:rpi-clock $BASE_PATH

# Install python requirements
sudo -u rpi-clock pip3 install -r $BASE_PATH/requirements.txt > /dev/null

# Configure paths
ln -f -s $BASE_PATH/rpi-clock.py /usr/local/bin/rpi-clock
ln -f -s $BASE_PATH/rpi-clock-update.sh /usr/local/bin/rpi-clock-update
ln -f -s $BASE_PATH/systemd/rpi-clock.service /etc/systemd/system/rpi-clock.service
ln -f -s $BASE_PATH/systemd/rpi-clock-update.service /etc/systemd/system/rpi-clock-update.service
ln -f -s $BASE_PATH/systemd/rpi-clock-update.timer /etc/systemd/system/rpi-clock-update.timer
chmod +x /usr/local/bin/{rpi-clock,rpi-clock-update}

# Enable and restart service
systemctl daemon-reload
systemctl enable rpi-clock.service rpi-clock-update.timer > /dev/null
systemctl restart rpi-clock.service
