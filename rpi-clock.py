#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from time import sleep
from tm1637 import TM1637
import adafruit_dht
import board
import RPi.GPIO as gpio
import asyncio
from multiprocessing import Process, Value

DISPLAY_CLOCK_PIN=25
DISPLAY_DIO_PIN=8
DHT_DATA_PIN=board.D23
SWITCH_PIN=5
temperature = 20

def update_temperature(temperature):
    dht = adafruit_dht.DHT22(DHT_DATA_PIN)
    while True:
        try:
            temperature.value = dht.temperature
            print("New temp: %f"%temperature.value)
        except:
            pass
        sleep(5)

def update_display(temperature):
    display = TM1637(clk=DISPLAY_CLOCK_PIN, dio=DISPLAY_DIO_PIN)
    display.brightness(4)
    gpio.setup(SWITCH_PIN, gpio.IN)
    while True:
        if bool(gpio.input(SWITCH_PIN)): # Display time
            now = datetime.now()
            display.numbers(now.hour, now.minute, colon=bool(now.second%2))
        else: # Display temperature
            display.dec_temperature(temperature.value)
        sleep(1)

def main():
    temperature = Value('d', 20)
    Process(target=update_temperature, args=(temperature,)).start()
    Process(target=update_display, args=(temperature,)).start()

if __name__ == "__main__":
    main()
